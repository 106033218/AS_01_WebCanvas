var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');

/*---------------------------------------------初始設定-----------------------------------------------------*/
ctx.globalCompositeOperation = "source-over";
ctx.lineJoin='round';   //兩條線交會處產生圓形邊角
ctx.lineCap='round';    //筆觸預設為圓形
Bsize();                //初始化筆刷大小
var mode = 0; //0是筆，1是橡皮擦，2是打字，3是圓形，4是矩形，5是三角形
canvas.style.cursor = "url('icon_pen.png'), default";

let hue=0;              //色相環

var FontSize=20;
var FontEffect="normal";
var FontType="Arial";

/*---------------------------------------------undo、redo相關-----------------------------------------------------*/
var canvasPic = new Image();
var cPushArray = new Array();
var cStep = -1;
cPush();

//儲存當前畫面
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('art').toDataURL());
}

//上一步
function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

//下一步
function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}


/*---------------------------------------------筆、橡皮擦、形狀相關-----------------------------------------------------*/
//選擇筆刷、形狀、字體等等的顏色
function color(){
    var x = document.getElementById("colorPicker").value;
    ctx.strokeStyle = x;
}

//選擇筆刷、橡皮擦、形狀的粗細
function Bsize(){
    var x = document.getElementById("b-size").value;
    ctx.lineWidth = x;
}

//切換模式
function modeChange(x){
    switch(x){
        case 0:     //筆
            canvas.style.cursor = "url('icon_pen.png'), pointer";
            break;
        case 1:     //橡皮擦
            canvas.style.cursor = "url('icon_eraser.png'), pointer";
            break;
        case 2:     //打字
            canvas.style.cursor = "url('icon_type.png'), pointer";
            break;
        case 3:     //畫圓
            canvas.style.cursor = "url('icon_circle.png'), pointer";
            break;
        case 4:     //畫矩形
            canvas.style.cursor = "url('icon_rectangle.png'), pointer";
            break;
        case 5:     //畫三角形
            canvas.style.cursor = "url('icon_triangle.png'), pointer";
            break;
        case 6:     //彩虹筆
            canvas.style.cursor = "url('icon_rainbow.png'), pointer";
            break;
        default:
            break;
    }
    mode = x;
}

//獲取滑鼠在canvas中的位置
function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

//用來畫線或橡皮擦
function mouseMove(evt) {
    var mousePos = getMousePos(canvas, evt);
    if(mode==6){
        ctx.strokeStyle=`hsl(${hue}, 100%, 50%)`;   //透過context的strokeStyle設定筆畫顏色
        hue++;  //色相環度數更新
        if(hue>=360){
            hue=0;
        }
    }
    else{
        ctx.strokeStyle=document.getElementById("colorPicker").value;
    }
    ctx.beginPath();
    ctx.moveTo(position.x, position.y);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
    position = mousePos;
}

var position;   //紀錄上個滑鼠位置

//畫圓形
function mouseMove_c(evt){
    canvasPic.src = cPushArray[cStep];            
    canvasPic.onload = function () { 
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(canvasPic, 0, 0); 
    }
    ctx.strokeStyle=document.getElementById("colorPicker").value;
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();
    ctx.arc(position.x, position.y, Math.sqrt(Math.pow(mousePos.x-position.x,2)+Math.pow(mousePos.y-position.y,2)), 0, 360, false);
    setTimeout(()=>{ctx.stroke()},1);
    ctx.closePath();
}

//畫矩形
function mouseMove_r(evt){
    canvasPic.src = cPushArray[cStep];            
    canvasPic.onload = function () { 
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(canvasPic, 0, 0); 
    }
    ctx.strokeStyle=document.getElementById("colorPicker").value;
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();
    ctx.rect(position.x, position.y, mousePos.x-position.x, mousePos.y-position.y);
    setTimeout(()=>{ctx.stroke()},1);
    ctx.closePath();
}

//畫三角形
function mouseMove_t(evt){
    canvasPic.src = cPushArray[cStep];            
    canvasPic.onload = function () { 
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(canvasPic, 0, 0); 
    }
    ctx.strokeStyle=document.getElementById("colorPicker").value;
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);
    ctx.lineTo(position.x, mousePos.y);
    ctx.lineTo((position.x+mousePos.x)/2, position.y);
    setTimeout(()=>{ctx.stroke()},1);
    ctx.closePath();
}

//偵測按下滑鼠
canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    if(mode==0 || mode==1 || mode==6){
        if(mode==0 || mode==6) ctx.globalCompositeOperation = "source-over";
        else if(mode==1) ctx.globalCompositeOperation = "destination-out";
        position = mousePos;
        ctx.moveTo(position.x, position.y);
        evt.preventDefault();
        canvas.addEventListener('mousemove', mouseMove, false);
    }
    else if(mode==2){
        text(evt);
    }
    else if(mode==3||mode==4||mode==5){
            position = mousePos;
            ctx.moveTo(position.x, position.y);
            evt.preventDefault();
            switch(mode){
                case 3:
                    canvas.addEventListener('mousemove', mouseMove_c, false);
                    break;
                case 4:
                    canvas.addEventListener('mousemove', mouseMove_r, false);
                    break;
                case 5:
                    canvas.addEventListener('mousemove', mouseMove_t, false);
                    break;
                default:
                    break;
            }
    }
});

//偵測放開滑鼠
canvas.addEventListener('mouseup', function() {
    if(mode!=2)cPush();
    ctx.globalCompositeOperation = "source-over";
    if(mode==0||mode==1||mode==6) canvas.removeEventListener('mousemove', mouseMove, false);
    else if(mode==3) canvas.removeEventListener('mousemove', mouseMove_c, false);
    else if(mode==4) canvas.removeEventListener('mousemove', mouseMove_r, false);
    else if(mode==5) canvas.removeEventListener('mousemove', mouseMove_t, false);
}, false);


/*---------------------------------------------打字相關-----------------------------------------------------*/
//獲取滑鼠在background的位置
function getMousePos_b(Background, evt) {
    var rect = Background.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
  }

//輸入文字
function text(evt){
    var Background = document.getElementById("canvasBackground");
    var position_b = getMousePos_b(Background, evt);
    var position = getMousePos(canvas, evt);
    var x = document.getElementById("colorPicker").value;
    var text=document.createElement("input");
    var text_area=document.getElementById("canvasBackground");
    text.style.position="absolute";
    text.style.top=`${position_b.y}px`;
    text.style.left=`${position_b.x}px`;
    text_area.appendChild(text);
    ctx.font=`${FontEffect} ${FontSize}px ${FontType}`;
    ctx.fillStyle=x;
    text.style.color=x;
    ctx.moveTo(position.x, position.y);
    ctx.beginPath();
    text.addEventListener("keydown", function(event){
        if(event.keyCode==13){
            ctx.fillText(`${text.value}`, position.x, position.y);
            ctx.stroke();
            text.remove();
            cPush();
        }
    });
    ctx.closePath();
}

//字體樣式
function fontEffect(fE){
    FontEffect=fE;
}
function fontType(fT){
    FontType=fT;
}
function fontSize(fs){
    FontSize=fs;
}


/*---------------------------------------------清空畫面-----------------------------------------------------*/
function clean(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    cPush();
}

/*---------------------------------------------下載圖片-----------------------------------------------------*/
function download(){
    var link = document.createElement("a");
    link.download = "canvas.png";
    link.href = canvas.toDataURL();
    link.click();
}

/*---------------------------------------------上傳圖片-----------------------------------------------------*/
function upload(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img,0,0);
            cPush();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}