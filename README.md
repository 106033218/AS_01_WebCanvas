# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Rainbow pen                                      | 1~5%      | Y         |


---

### How to use 

<img src="sample_01.png"></img>

    此網頁可以用各種不同工具，對中間空白區域(Canvas)進行繪圖，不同工具游標顯示會不同，功能包括：
    
    basic:
    1. 畫筆: 點擊標示後使用，可透過按住滑鼠移動在canvas上畫出線條。
    2. 橡皮擦: 點擊標示後使用，可透過按住滑鼠移動將canvas上的經過區域清空。
    3. 調色盤: 點擊後出現調色盤，並將選取的顏色設為"畫筆"、"文字"、"形狀工具"的顏色。
    4. 筆刷粗細控制: 藉由拖動range上的游標來控制筆刷粗度，會影響到"畫筆"、"橡皮擦"、"形狀工具"、"彩虹筆"的粗細。
    5. 文字輸入: 點擊標示後使用，可在canvas上任意位置輸入文字並打印上去。
    6. 文字字體選擇: 點擊後可選取需要的輸入文字字體。
    7. 文字字型選擇: 點擊後可選取需要的輸入文字字型。
    8. 文字大小選擇: 點擊後可選取需要的輸入文字大小。
    9. 清空畫面: 點擊後可清空canvas上所有內容。

    advanced:
    10. 圓形: 點擊標示後使用，可透過按住滑鼠移動在canvas上畫出圓形。
    11. 矩形: 點擊標示後使用，可透過按住滑鼠移動在canvas上畫出矩形。
    12. 三角形: 點擊標示後使用，可透過按住滑鼠移動在canvas上畫出三角形。
    13. 復原按鈕: 點擊後將canvas圖像返回上一狀態。
    14. 重作按鈕: 點擊後將canvas圖像重作回下一狀態。
    15. 圖片上傳工具: 點擊後可從本機檔案中上傳圖片至canvas中。
    16. 圖片下載工具: 點擊後可將canvas圖像下載至本機檔案中。

### Function description

    17. 彩虹筆: 點擊標示後使用，可透過按住滑鼠移動在canvas上畫出會變換顏色的線條。

### Gitlab page link

    https://106033218.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    助教們辛苦了~

<!-- <style>
table th{
    width: 100%;
}
</style> -->